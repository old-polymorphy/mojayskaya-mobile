export interface User {
  id: number;
  email: string;
  password: string;
  password_confirmation: string;
  created_at: string;
  updated_at: string;
  first_name: string;
  last_name: string;
  gender: string;
  company: string;
  position: string;
  facebook: string;
  phone: string;
  picture: string;
  role: string;
  cv: string;
}