import axios, { AxiosResponse } from 'axios';
import { Job } from '../interfaces/job';

const baseUrl = 'http://hr.polymorphy.ru';

import { AsyncStorage } from 'react-native';
import { User } from '../interfaces/user';

export default {
  async getVacancies(): Promise<AxiosResponse> {
    try {
      const accessToken = await AsyncStorage.getItem('access_token');

      return axios.get(`${baseUrl}/api/vacancies`, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });
    } catch (err) {
      console.log(err.message);
    }
  }
};
