export interface Post {
  subject: string,
  teaser: string,
  content: string,
  illustration: File,
  updated_at: string,
  created_at: string,
  id: number,
}
