// Framework
import React, {Component} from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView, StatusBar,
} from 'react-native';

// Styles
import common from '../styles/common';

// Services
import PopupMessageComponent from '../ui/PopupMessage';
import SvgUri from 'react-native-svg-uri';
import {ScrollView} from 'react-native-gesture-handler';

interface Props {
  error: string;

  title: string;

  goBack(): void;

  unsetError(): void;
}

export default class AuthLayout extends Component<Props> {
  public render(): React.ReactNode {
    return (

      <ImageBackground style={[common.container, styles.container]} source={require('../img/bg.png')}>

        <StatusBar barStyle={'light-content'}/>

        <View style={styles.back} onTouchEnd={this.props.goBack}>
          <SvgUri
            height={16}
            width={16}
            source={require('../img/back.svg')}
          />
        </View>

        <KeyboardAvoidingView style={{flex: 1, width: '100%'}} behavior='padding' enabled>

          <ScrollView style={styles.scrollView}>

            {/* Heroes */}
            <Image source={require('../img/heroes.png')} style={[common.heroes, styles.heroes]}/>

            {this.props.children}

          </ScrollView>

        </KeyboardAvoidingView>

        {this.renderPopup()}

        {/* Policy */}
        <View style={common.policyWrapper}>
          <Text style={common.policyText}>Политика конфиденциальности</Text>
        </View>

      </ImageBackground>
    );
  }

  private renderPopup = (): React.ReactNode => {
    if (!this.props.error) {
      return null;
    }

    return (
      <PopupMessageComponent
        type={'error'}
        text={this.props.error}
        close={this.props.unsetError}
      />
    );
  };
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    width: '100%',
  },
  back: {
    position: 'absolute',
    top: 40,
    left: 20,

    height: 16,
    width: 16,

    zIndex: 200,
  },
  container: {
    justifyContent: 'flex-start',
    height: '100%',
  },
  heroes: {
    width: '85%',
  },
  headingText: {
    marginBottom: 21,
    textAlign: 'center',
    fontSize: 24,
    color: '#fff',
  },
  inputField: {
    borderColor: '#fff',
    borderWidth: 1,
    color: '#fff',
    paddingLeft: 20,
  }
});
