// Framework
import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

// Libs
import { AxiosResponse } from 'axios';

// UI
import { Header } from './ui/Header';

// Styles
import common from './styles/common';

// Interfaces
import { Job } from './interfaces/job';

// Services
import jobService from './services/jobs';

/**
 * Describes the component props.
 */
interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

/**
 * Describes the component state.
 */
interface State {
  /**
   * Vacancy list.
   * Displayed only for users of the
   * particular type.
   */
  vacancies: Job[];
}

export class JobsScreen extends Component<Props, State> {
  public state = { vacancies: [] };

  static navigationOptions = {
    title: 'Кадры',
  };

  /** @inheritDoc */
  public async componentDidMount(): Promise<void> {
    try {
      const response = await jobService.getVacancies() as AxiosResponse<Job[]>;

      this.setState({ vacancies: response.data });
    } catch (e) {

    }
  }

  public render(): React.ReactNode {
    return (
      <View style={{ flex: 1 }}>

        {/* Header */}
        <Header
          title={'Кадры'}
          button={this.renderButton()}
          goBack={this.props.navigation.goBack}
        />

        {/* Container */}
        <ScrollView>
          {this.renderJobs()}
        </ScrollView>

      </View>
    );
  }

  /**
   * Renders the header button to pass
   * to the "Header" component.
   */
  private renderButton = (): React.ReactNode => {
    return (
      <View style={common.headerButton}>
        <Text style={common.headerButtonText}>Загрузить резюме</Text>
      </View>
    );
  };

  /**
   * Renders all jobs. Both vacancies and candidates.
   */
  private renderJobs = (): React.ReactNode => {
    return this.state.vacancies.map((vacancy: Job, index: number) => {
      return (
        <View key={index} style={styles.job}>

          {/* Heading*/}
          <View style={styles.jobHeading}>
            <Text style={styles.jobTitle}>{vacancy.position}</Text>
            <SvgUri height={27} width={27} source={require('./img/star.svg')}/>
          </View>

          {/* Description */}
          <Text style={styles.jobDescription}>{vacancy.description}</Text>

          {/* Salary block */}
          <View style={styles.jobSalary}>
            <Text style={styles.jobSalaryText}>до {vacancy.salary}</Text>
            <View style={styles.jobMoreButton}><Text style={styles.jobsMoreText}>Подробнее о вакансии</Text></View>
          </View>

        </View>
      );
    });
  };
}

const styles = StyleSheet.create({
  job: {
    // Flex
    display: 'flex',

    // Spacing
    padding: 20,
  },
  jobHeading: {
    // Flex
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

    // Spacing
    marginBottom: 8,
    paddingBottom: 9,

    // Border
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(151, 151, 151, 0.1)',
  },
  jobTitle: {
    // Text
    fontSize: 20,
    lineHeight: 20,
  },
  jobDescription: {
    // Text
    fontSize: 12,
    lineHeight: 18
  },
  jobSalary: {
    // Flex
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

    // Spacing
    marginTop: 22,
  },
  jobSalaryText: {
    // Text
    fontSize: 22,
    // fontWeight: 'bold',
  },
  jobMoreButton: {
    // Flex
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    // Spacing
    paddingRight: 15,
    paddingLeft: 15,

    // Size
    height: 32,

    // Colors
    backgroundColor: '#0082FD',

    // Border
    borderRadius: 16,
  },
  jobsMoreText: {
    // Text
    color: '#fff',
    fontSize: 12,
    lineHeight: 14,
  }
});
