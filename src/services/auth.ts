import axios, {AxiosResponse} from 'axios';
import {User} from '../interfaces/user';

const baseUrl = 'http://hr.polymorphy.ru';

import {AsyncStorage} from 'react-native';

export default {
  logIn(email: string, password: string): Promise<AxiosResponse> {
    return axios.post(`${baseUrl}/api/auth/login`, {email, password}, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    });
  },

  signUp(user: Partial<User>): Promise<AxiosResponse> {
    const {email, first_name, last_name, company, position, password, password_confirmation, phone} = user;

    return axios.post(`${baseUrl}/api/auth/signup`, {
      email, first_name, last_name, company, position, password, password_confirmation, phone
    }, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    });
  },

  async logOut(): Promise<AxiosResponse> {
    try {
      const accessToken = await AsyncStorage.getItem('access_token');

      return axios.get(`${baseUrl}/api/auth/logout`, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });
    } catch (err) {
      console.log(err.message);
    }
  },

  async profile(): Promise<AxiosResponse> {
    try {
      const accessToken = await AsyncStorage.getItem('access_token');

      return axios.get(`${baseUrl}/api/user`, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });
    } catch (err) {
      console.log(err.message);
    }
  },

  async updateProfile(user: Partial<User>): Promise<AxiosResponse> {
    try {
      const accessToken = await AsyncStorage.getItem('access_token');

      return axios.post(`${baseUrl}/api/user`, {...user}, {
        headers: {
          'Authorization': `Bearer ${accessToken}`,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });
    } catch (err) {
      console.log(err.message);
    }
  }
};