import React from 'react';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import SignInScreen from './src/SignIn';
import { HomeScreen } from './src/Home';
import { FeedScreen } from './src/Feed';
import SignUpScreen from './src/SignUp';
import { ProfileScreen } from './src/Profile';
import Tabs from './src/Tabs';

const MainNavigator = createStackNavigator({
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        header: null,
      }
    },
    SignIn: {
      screen: SignInScreen,
      navigationOptions: {
        header: null,
      }
    },
    SignUp: {
      screen: SignUpScreen,
      navigationOptions: {
        header: null,
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        header: null,
      }
    },
    Tabs: {
      screen: Tabs,
      navigationOptions: {
        header: null,
      }
    }
  }
);

const App = createAppContainer(MainNavigator);

export default App;
