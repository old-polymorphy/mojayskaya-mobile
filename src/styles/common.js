import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  back: {
    // Spacing
    marginRight: 18,

    // Size
    height: 16,
    width: 16,
  },
  container: {
    // Flex
    display: 'flex',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between',

    // Spacing
    padding: '8%',
    paddingTop: 16,
    paddingBottom: 16,
  },
  heroes: {
    width: '90%',
    resizeMode: 'contain',
  },
  buttonWrapper: {
    width: '100%',
  },
  commonButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 28,
    height: 56,
    width: '100%',
    marginBottom: 18
  },
  signInWrapper: {
    backgroundColor: '#fff',
  },
  signInText: {
    color: '#3051c0',
    fontSize: 16,
  },
  recoveryWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: 'auto',
  },
  recoveryText: {
    color: '#fff',
    marginLeft: 5,
    marginRight: 5,
  },
  policyWrapper: {},
  policyText: {
    marginTop: 'auto',
    opacity: 0.5,
    color: '#fff',
    fontSize: 12,
  },
  validationError: {
    marginBottom: 12,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  validationErrorText: {
    marginLeft: 4,
    color: 'white',
    fontSize: 12,
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 32,
    height: 96,
    backgroundColor: '#0082FD',
    color: '#fff',
    borderBottomRightRadius: 32,
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    lineHeight: 23,
  },
  headerButton: {
    display: 'flex',
    justifyContent: 'center',
    padding: 15,
    paddingTop: 0,
    paddingBottom: 0,
    height: 32,
    backgroundColor: '#fff',
    borderRadius: 16,
  },
  headerButtonText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#296FCD',
  }
});
