export interface AuthObject {
  access_token: string;
  token_type: string;
  expires_at: string;
}