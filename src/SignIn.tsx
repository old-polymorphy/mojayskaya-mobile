// Framework
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';

// Styles
import common from './styles/common';

// Services
import authService from './services/auth';
import {AxiosError, AxiosResponse} from 'axios';

import {AsyncStorage} from 'react-native';
import {AuthObject} from './interfaces/auth';
import AuthLayout from './ui/AuthLayout';
import SvgUri from 'react-native-svg-uri';

interface State {
  email: string;
  error: string;
  password: string;
  errors: {
    [key: string]: string;
  }
}

export default class SignInScreen extends Component<{}, State> {
  static navigationOptions = {
    title: '',
    headerStyle: {
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
    }
  };

  public state = {
    email: '',
    error: '',
    password: '',
    errors: {},
  };

  public render(): React.ReactNode {
    return (
      <AuthLayout
        title={'Авторизация'}
        error={this.state.error}
        goBack={this.goBack}
        unsetError={this.unsetError}
      >

        {/* Form */}
        <View style={common.buttonWrapper}>

          {/* Authorization */}
          <Text style={styles.headingText}>Авторизация</Text>

          <TextInput
            style={[common.commonButton, styles.inputField]}
            keyboardType={'email-address'}
            placeholder={'Введите email'}
            placeholderTextColor={'#fff'}
            value={this.state.email}
            onChangeText={this.handleEmailInput}
            onFocus={this.resetErrors}
          />
          {this.renderError('email')}

          <TextInput
            style={[common.commonButton, styles.inputField]}
            secureTextEntry={true}
            placeholder={'Пароль'}
            placeholderTextColor={'#fff'}
            value={this.state.password}
            onChangeText={this.handlePasswordInput}
            onFocus={this.resetErrors}
          />
          {this.renderError('password')}

          {/* Sign In Button */}
          <View style={[common.commonButton, common.signInWrapper]} onTouchEnd={this.handleSubmit}>
            <Text style={common.signInText}>Войти</Text>
          </View>

          {/* Recovery Link */}
          <View style={common.recoveryWrapper}>
            <Text style={common.recoveryText}>Забыли пароль?</Text>
            <Text style={common.recoveryText}>Восстановить</Text>
          </View>

        </View>

      </AuthLayout>
    );
  }

  private handleEmailInput = (text: string): void => {
    this.setState({
      email: text,
    });
  };

  private errorFieldStyle = (filedName: string) => {
    if (this.state.errors[filedName]) {
      return {
        borderColor: '#F04438',
      };
    }
  };

  private unsetError = (): void => {
    this.setState({error: ''});
  };

  private resetErrors = (): void => {
    this.setState({errors: {}});
  };

  private handlePasswordInput = (text: string): void => {
    this.setState({
      password: text,
    });
  };

  private renderError = (fieldName: string): React.ReactNode => {
    if (this.state.errors[fieldName]) {
      return (
        <View style={common.validationError}>
          <SvgUri height={16} width={16} source={require('./img/error.svg')}/>
          <Text style={common.validationErrorText}>{this.state.errors[fieldName]}</Text>
        </View>
      )
    }
  };

  private handleSubmit = (): void => {
    authService.logIn(this.state.email, this.state.password)
      .then(async (response: AxiosResponse<AuthObject>): Promise<void> => {
        const authObject: AuthObject = response.data;

        try {
          await AsyncStorage.setItem('access_token', authObject.access_token);
          await AsyncStorage.setItem('token_type', authObject.token_type);
          await AsyncStorage.setItem('expires_at', authObject.expires_at);

          const {navigate} = this.props.navigation;

          navigate('Tabs');
        } catch (e) {
          this.setState({error: e.message});
        }
      })
      .catch((err: AxiosError) => {
        switch (err.response.status) {
          case 422:
            this.setState({errors: err.response.data.errors});
            break;
          case 401:
            this.setState({error: 'Неверный логин или пароль'});
            break;
          default:
            this.setState({error: err.message});
        }
      });
  };

  private goBack = (): void => {
    const {goBack} = this.props.navigation;
    goBack(null);
  };
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    height: '100%',
  },
  heroes: {
    width: '75%',
  },
  headingText: {
    marginBottom: 21,
    textAlign: 'center',
    fontSize: 24,
    color: '#fff',
  },
  inputField: {
    borderColor: '#fff',
    borderWidth: 1,
    color: '#fff',
    paddingLeft: 20,
  }
});
