// Freamwork
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SvgUri from 'react-native-svg-uri';

// Styles
import common from '../styles/common';

interface Props {
  title: string;
  back?: boolean;
  button?: React.ReactNode;

  goBack(routeKey?: string | null): boolean;
}

export class Header extends Component<Props> {

  /** @inheritDoc */
  public render(): React.ReactNode {
    return (
      <View style={common.header}>

        {/* Back */}
        {this.renderBackButton()}

        {/* Text */}
        <Text style={common.headerText}>{this.props.title}</Text>

        {/* Spacer */}
        <View style={{ flex: 1 }}/>

        {/* Button */}
        {this.props.button}

      </View>
    );
  }

  /**
   * Renders the "back" button
   * if the prop us passed.
   */
  private renderBackButton = (): React.ReactNode => {
    if (!this.props.back) {
      return null;
    }

    return (
      <View style={common.back} onTouchEnd={this.handleGoBack}>
        <SvgUri
          height={16}
          width={16}
          source={require('../img/back.svg')}
        />
      </View>
    );
  };

  private handleGoBack = (): void => {
    this.props.goBack(null);
  };
}
