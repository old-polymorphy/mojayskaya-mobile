// Framework
import React, { Component } from 'react';
import { Dimensions, Image, ImageBackground, StatusBar, StyleSheet, Text, View } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import { AsyncStorage } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

// Styles
import common from './styles/common';

/**
 * Describes the component props.
 */
interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

// Constants
const win = Dimensions.get('window');
const logoHeight = (win.width * 0.84) / 5;
export const heroesHeight = (win.width * 0.84 * 0.9) / 1.206896552;

export class HomeScreen extends Component<Props> {
  public async componentDidMount(): Promise<void> {
    // await AsyncStorage.clear();

    try {
      const accessToken = await AsyncStorage.getItem('access_token');
      const expiresAt = await AsyncStorage.getItem('expires_at');

      if (accessToken) {
        this.props.navigation.navigate('Tabs');
      }
    } catch (err) {
      console.log(err);
    }
  }

  public render(): React.ReactNode {
    return (
      <ImageBackground source={require('./img/bg.png')} style={common.container}>

        <StatusBar barStyle={'light-content'}/>

        {/* Heroes illustration */}
        <Image source={require('./img/heroes.png')} style={[common.heroes, styles.heroes]}/>

        {/* Logo */}
        <Image source={require('./img/logo.png')} style={styles.textLogo}/>

        {/* Buttons */}
        <View style={common.buttonWrapper}>

          {/* Facebook Button */}
          <View onTouchEnd={this.handleSignIn} style={[common.commonButton, styles.facebookWrapper]}>

            {/* Facebook Icon */}
            <View style={styles.facebookIcon}>
              <SvgUri height={36} width={36} source={require('./img/facebook.svg')}/>
            </View>

            {/* Button text*/}
            <Text style={styles.facebookText}>Продолжить с Facebook</Text>

          </View>

          {/* Sign In Button */}
          <View onTouchEnd={this.handleSignIn} style={[common.commonButton, common.signInWrapper]}>
            <Text style={common.signInText}>Войти</Text>
          </View>

          {/* Recovery Link */}
          <View style={common.recoveryWrapper} onTouchEnd={this.handleSignUp}>
            <Text style={common.recoveryText}>Нет аккаунта?</Text>
            <Text style={common.recoveryText}>Зарегистрироваться</Text>
          </View>

        </View>

        {/* Spacer */}
        <View style={{ flex: 1 }}/>

        {/* About us */}
        <View style={styles.aboutWrapper}>
          <Text style={styles.aboutText}>О нас</Text>
        </View>

        {/* Spacer */}
        <View style={{ flex: 1 }}/>

        {/* Policy */}
        <View style={common.policyWrapper}>
          <Text style={common.policyText}>Политика конфиденциальности</Text>
        </View>

      </ImageBackground>
    );
  }

  private handleSignIn = (): void => {
    const { navigate } = this.props.navigation;

    navigate('SignIn');
  };

  private handleSignUp = (): void => {
    const { navigate } = this.props.navigation;

    navigate('SignUp');
  };
}

const styles = StyleSheet.create({
  facebookWrapper: {
    backgroundColor: '#273E89',
  },
  facebookIcon: {
    position: 'absolute',
    left: 10,
  },
  facebookText: {
    color: '#fff',
    fontSize: 16,
  },

  // Text logo
  textLogo: {
    height: logoHeight,
    width: '100%',

    marginBottom: 54,

    resizeMode: 'contain',
  },

  // Illustration
  illustration: {
    maxWidth: 256,
  },

  // About button
  aboutWrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.25)',
    borderRadius: 22,
    height: 44,
    width: 135,
  },
  aboutText: {
    fontSize: 16,
    color: '#fff',
  },

  // Heroes illustration
  heroes: {
    height: heroesHeight,
  }
});
