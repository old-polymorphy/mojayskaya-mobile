import { Condition } from './condition';

export interface Job {
  position: string;
  salary: string;
  description: string;
  tasks: string;
  requirements: string;
  updated_at: string;
  created_at: string;
  id: number;
  conditions: Condition[];
}
