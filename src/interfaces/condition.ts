export interface Condition {
  id: number,
  title: string,
  created_at: string,
  updated_at: string,
  deleted_at: string,
  pivot?: {
    vacancy_id: number,
    condition_id: number,
  }
}
