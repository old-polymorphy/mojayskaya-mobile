export default {
  // Sizes
  height: 82,

  // Border
  borderTopWidth: 0,
  borderTopLeftRadius: 23,
  borderTopRightRadius: 23,

  // Shadow
  shadowColor: 'rgb(10, 16, 21)',
  shadowOffset: { width: 0, height: 0 },
  shadowOpacity: 0.161877,
  shadowRadius: 17,
}
