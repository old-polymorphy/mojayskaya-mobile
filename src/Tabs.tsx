import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

// Styles
import bottom from './styles/bottom';

// Screens
import { JobsScreen } from './Jobs';
import { FeedScreen } from './Feed';

const TabNavigator = createBottomTabNavigator({
  Feed: FeedScreen,
  Jobs: JobsScreen,
}, {
  tabBarOptions: {
    activeTintColor: '#e91e63',
    labelStyle: {
      fontSize: 12,
    },
    style: bottom,
  }
});

export default createAppContainer(TabNavigator);
