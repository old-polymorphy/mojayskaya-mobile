// Framework
import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

// Message types
declare type MessageTypes = 'error' | 'success' | 'info';

/**
 * Describes the component props.
 */
interface Props {
  /**
   * Popup message type.
   */
  type: MessageTypes;

  /**
   * Popup message inner text.
   */
  text: string;

  /**
   * Close popup callback.
   */
  close(): void;
}

export default class PopupMessageComponent extends Component<Props> {
  public static dict = {
    error: {
      icon: require(`../img/error-icon.png`),
      text: require(`../img/error-text.png`),
    }
  };

  /** @inheritDoc */
  public render(): React.ReactNode {

    const dict = PopupMessageComponent.dict[this.props.type];

    return (
      <View style={styles.popupWrapper} onTouchEnd={this.props.close}>
        <View style={styles.popupContainer}>
          <Image source={dict.icon} style={styles.popupIcon}/>
          <Image source={dict.text} style={styles.popupText}/>
          <Text>{this.props.text}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  popupWrapper: {
    display: 'flex',

    alignItems: 'center',
    justifyContent: 'center',

    position: 'absolute',

    zIndex: 100,

    top: 0,
    right: 0,
    bottom: 0,
    left: 0,

    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  popupContainer: {
    display: 'flex',

    alignItems: 'center',

    padding: 24,
    borderRadius: 32,
    width: '90%',

    backgroundColor: '#fff',
  },
  popupIcon: {
    marginBottom: 14,
    height: 68,
    width: 68,
  },
  popupText: {
    margin: 'auto',
    marginBottom: 14,

    height: 29,
    width: 86,

    resizeMode: 'contain',
  }
});