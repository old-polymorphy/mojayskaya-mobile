// Framework
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

// UI
import { Header } from './ui/Header';

// Interfaces
import { User } from './interfaces/user';
import { AxiosResponse } from 'axios';

// Services
import authService from './services/auth';

/**
 * Describes the component props.
 */
interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

/**
 * Describes the component state.
 */
interface State {
  user?: Partial<User>;
}

export class FeedScreen extends Component<Props, State> {
  /** @inheritDoc */
  public state: State = {};

  /** @inheritDoc */
  public async componentDidMount(): Promise<void> {
    try {
      const response = await authService.profile() as AxiosResponse<User>;
      this.setState({ user: response.data });
    } catch (err) {
      console.error(err);
    }
  }

  /** @inheritDoc */
  public render(): React.ReactNode {
    return (
      <View>
        <Header
          title={this.buildTitle()}
          button={this.renderButton()}
          goBack={this.props.navigation.goBack}
        />
      </View>
    );
  }

  /**
   * Returns greeting.
   */
  private buildTitle = (): string => {
    if (!this.state.user) {
      return 'Здравствуйте!';
    }

    return `Здравствуйте, ${this.state.user.first_name}!`;
  };

  /**
   * Renders button that opens the profile page.
   */
  private renderButton = (): React.ReactNode => {
    // If there's no user
    if (!this.state.user) {
      return null;
    }

    // If the user has no picture
    if (!this.state.user.picture) {
      return (
        <View style={styles.pictureWrapper} onTouchEnd={this.handlePictureClick}>
          <Text>Н</Text>
        </View>
      );
    }

    const base64 = `data:image/png;base64,${this.state.user.picture}`;

    return (
      <View style={styles.pictureWrapper} onTouchEnd={this.handlePictureClick}>
        <Image style={styles.picture} source={{ uri: base64 }}/>
      </View>
    );
  };

  /**
   * Handles picture clicks.
   * Redirect to the profile.
   */
  private handlePictureClick = (): void => {
    this.props.navigation.navigate('Profile');
  };
}

// Styles
const styles = StyleSheet.create({
  pictureWrapper: {
    // Sizes
    height: 38,
    width: 38,
  },
  picture: {
    // Sizes
    height: 35,
    width: 35,

    // Border
    borderColor: '#fff',
    borderRadius: 19,
    borderWidth: 1.5,
  }
});
