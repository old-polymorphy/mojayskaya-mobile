import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {User} from '../interfaces/user';

interface Props {
  title: string;
  field: string;
  user: Partial<User>;
  placeholder?: string;
  secure?: boolean;

  handleInput(field: string): (text: string) => Promise<void>;

  handleSubmit(): Promise<void>;
}

interface State {
  disabled: boolean;
}

export class BigField extends Component<Props, State> {
  public state = {disabled: true};

  public render(): React.ReactNode {
    return (
      <View style={styles.field}>
        <Text style={styles.fieldSub}>{this.props.title}</Text>
        <TextInput
          editable={!this.state.disabled}
          value={this.props.user[this.props.field]}
          placeholder={this.props.placeholder}
          style={[styles.fieldInput, this.enabledStyle()]}
          secureTextEntry={this.props.secure}
          onChangeText={this.props.handleInput(this.props.field)}
          onBlur={this.handleBlur}
        />

        {/* Change button */}
        {this.renderChangeButton()}
      </View>
    );
  }

  private renderChangeButton = (): React.ReactNode => {
    if (!this.state.disabled) {
      return null;
    }

    return (
      <View style={styles.changeButton} onTouchEnd={this.enable}>
        <Text style={styles.changeButtonText}>Изменить</Text>
      </View>
    );
  };

  private handleBlur = async (): Promise<void> => {
    this.disable();
    await this.props.handleSubmit();
  };

  private enable = (): void => {
    this.setState({disabled: false});
  };

  private disable = (): void => {
    this.setState({disabled: true});
  };

  private enabledStyle = (): any => {
    if (this.state.disabled) {
      return null;
    }

    return {
      color: '#030F09',
    };
  };
}

const styles = StyleSheet.create({
  field: {
    // Position
    position: 'relative',

    // Spacing
    marginTop: 22,
  },
  fieldInput: {
    // Spacing
    padding: 20,

    // Sizes
    height: 56,

    // Border
    borderColor: '#AEAEAE',
    borderRadius: 28,
    borderWidth: 1,

    // Text
    color: '#8F8F8F',
    fontSize: 16,
  },
  fieldSub: {
    // Spacing
    marginBottom: 9,
    paddingLeft: 22,

    // Text
    color: '#8F8F8F',
    fontSize: 14,
  },
  changeButton: {
    // Position
    position: 'absolute',
    right: 12,
    bottom: 12,

    // Spacing
    padding: 16,
    paddingTop: 8,
    paddingBottom: 8,

    // Size
    height: 32,
    borderRadius: 16,

    // Background
    backgroundColor: '#0082FD',
  },
  changeButtonText: {
    // Text
    color: '#fff',
    fontSize: 12,
    fontWeight: 'bold',
  }
});