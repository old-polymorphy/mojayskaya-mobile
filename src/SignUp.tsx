// Framework
import React, { Component, ComponentState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import SvgUri from 'react-native-svg-uri';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';

// Styles
import common from './styles/common';

// Services
import authService from './services/auth';

// UI
import AuthLayout from './ui/AuthLayout';

// Interfaces
import { User } from './interfaces/user';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface State {
  stage: number;
  email: string;
  error: string;
  password: string;
  passwordRepeat: string;
  phone: string;
  firstName: string;
  lastName: string;
  company: string;
  position: string;
  errors: {
    [key: string]: string;
  }
}

export default class SignUpScreen extends Component<Props, State> {
  static navigationOptions = {
    title: '',
    headerStyle: {
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
    }
  };

  public state = {
    stage: 1,
    email: '',
    error: '',
    password: '',
    passwordRepeat: '',
    phone: '',
    errors: {},
    firstName: '',
    lastName: '',
    company: '',
    position: '',
  };

  public render(): React.ReactNode {
    return (
      <AuthLayout
        title={'Регистрация'}
        error={this.state.error}
        goBack={this.goBack}
        unsetError={this.unsetError}
      >

        {/* Form */}
        <View style={common.buttonWrapper}>

          {/* Circles */}
          {this.renderStageCircles()}

          {/* Authorization */}
          <Text style={styles.headingText}>Регистрация</Text>

          {this.renderStage()}

        </View>

      </AuthLayout>
    );
  }

  private handleEmailInput = (text: string): void => {
    this.setState({
      email: text,
    });
  };

  private errorFieldStyle = (filedName: string) => {
    if (this.state.errors[filedName]) {
      return {
        borderColor: '#F04438',
      };
    }
  };

  private renderStageCircles = (): React.ReactNode => {
    const circles = [1, 2, 3].map((stage: number) => {
      return (
        <View key={stage} style={[styles.circle, this.activeCircleStyle(stage)]}/>
      );
    });

    return (
      <View style={styles.circleWrapper}>
        {circles}
      </View>
    );
  };

  private activeCircleStyle = (stage: number): any => {
    if (stage === this.state.stage) {
      return {
        backgroundColor: '#fff',
      };
    }
  };

  private renderStage = (): React.ReactNode => {
    switch (this.state.stage) {
      case 1: {
        return (
          <>
            {/* Sub text */}
            <Text style={styles.subText}>Пожалуйста, введите свой адрес электронной почты</Text>

            <TextInput
              style={[common.commonButton, styles.inputField]}
              keyboardType={'email-address'}
              placeholder={'Ваш email'}
              placeholderTextColor={'#fff'}
              value={this.state.email}
              onChangeText={this.handleInput('email')}
              onFocus={this.resetErrors}
            />
            {this.renderError('email')}

            {/* Sign In Button */}
            <View style={[common.commonButton, common.signInWrapper]} onTouchEnd={this.nextStage}>
              <Text style={common.signInText}>Продолжить</Text>
            </View>
          </>
        );
      }
      case 2: {
        return (
          <>
            <TextInput
              style={[common.commonButton, styles.inputField]}
              placeholder={'Имя *'}
              placeholderTextColor={'#fff'}
              value={this.state.firstName}
              onChangeText={this.handleInput('firstName')}
              onFocus={this.resetErrors}
            />
            {this.renderError('firstName')}

            <TextInput
              style={[common.commonButton, styles.inputField]}
              placeholder={'Фамилия *'}
              placeholderTextColor={'#fff'}
              value={this.state.lastName}
              onChangeText={this.handleInput('lastName')}
              onFocus={this.resetErrors}
            />
            {this.renderError('lastName')}

            <TextInput
              style={[common.commonButton, styles.inputField]}
              keyboardType={'phone-pad'}
              placeholder={'Телефон *'}
              placeholderTextColor={'#fff'}
              value={this.state.phone}
              onChangeText={this.handleInput('phone')}
              onFocus={this.resetErrors}
            />
            {this.renderError('phone')}

            <TextInput
              style={[common.commonButton, styles.inputField]}
              placeholder={'Название вашей компании'}
              placeholderTextColor={'#fff'}
              value={this.state.company}
              onChangeText={this.handleInput('company')}
              onFocus={this.resetErrors}
            />
            {this.renderError('company')}

            <TextInput
              style={[common.commonButton, styles.inputField]}
              placeholder={'Ваша должность'}
              placeholderTextColor={'#fff'}
              value={this.state.position}
              onChangeText={this.handleInput('position')}
              onFocus={this.resetErrors}
            />
            {this.renderError('position')}

            {/* Sign In Button */}
            <View style={[common.commonButton, common.signInWrapper]} onTouchEnd={this.nextStage}>
              <Text style={common.signInText}>Продолжить</Text>
            </View>
          </>
        );
      }
      case 3: {
        return (
          <>
            <TextInput
              style={[common.commonButton, styles.inputField]}
              secureTextEntry={true}
              placeholder={'Введите пароль'}
              placeholderTextColor={'#fff'}
              value={this.state.password}
              onChangeText={this.handleInput('password')}
              onFocus={this.resetErrors}
            />
            {this.renderError('password')}

            <TextInput
              style={[common.commonButton, styles.inputField]}
              secureTextEntry={true}
              placeholder={'Повторите пароль'}
              placeholderTextColor={'#fff'}
              value={this.state.passwordRepeat}
              onChangeText={this.handleInput('passwordRepeat')}
              onFocus={this.resetErrors}
            />
            {this.renderError('passwordRepeat')}

            {/* Sign In Button */}
            <View
              style={[common.commonButton, common.signInWrapper, this.renderInactiveSubmitStyle()]}
              onTouchEnd={this.handleSubmit}
            >
              <Text style={common.signInText}>Завершить регистрацию</Text>
            </View>
          </>
        );
      }
    }
  };

  private unsetError = (): void => {
    this.setState({ error: '' });
  };

  private resetErrors = (): void => {
    this.setState({ errors: {} });
  };

  private isDisabled = (): boolean => {
    return !this.state.password || !this.state.passwordRepeat;
  };

  private renderInactiveSubmitStyle = (): any => {
    if (this.isDisabled()) {
      return {
        opacity: 0.47,
      };
    }
  };

  private renderError = (fieldName: string): React.ReactNode => {
    if (this.state.errors[fieldName]) {
      return (
        <View style={common.validationError}>
          <SvgUri height={16} width={16} source={require('./img/error.svg')}/>
          <Text style={common.validationErrorText}>{this.state.errors[fieldName]}</Text>
        </View>
      );
    }
  };

  private nextStage = (): void => {
    this.setState({ stage: this.state.stage + 1 });
  };

  private handleInput = (fieldName: string) => (text: string): void => {
    this.setState({
      [fieldName]: text,
    } as ComponentState);
  };

  private handleSubmit = async (): Promise<void> => {
    if (this.isDisabled()) {
      return;
    }

    const user: Partial<User> = {
      email: this.state.email,
      first_name: this.state.firstName,
      last_name: this.state.lastName,
      company: this.state.company,
      position: this.state.position,
      password: this.state.password,
      password_confirmation: this.state.passwordRepeat,
      phone: this.state.phone,
    };

    try {
      const response = await authService.signUp(user);

      if (response.status === 201) {
        const { navigate } = this.props.navigation;

        navigate('SignIn');
      }
    } catch (err) {
      switch (err.response.status) {
        case 422:
          this.setState({ errors: err.response.data.errors, stage: 1 });
          break;
        default:
          this.setState({ error: err.message });
      }
    }
  };

  private goBack = (): void => {
    const { goBack } = this.props.navigation;
    goBack(null);
  };
}

const styles = StyleSheet.create({
  circleWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15,
  },
  circle: {
    marginRight: 9,
    height: 10,
    width: 10,
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.26)'
  },
  subText: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 16,
    color: '#fff',
  },
  headingText: {
    marginBottom: 21,
    textAlign: 'center',
    fontSize: 24,
    color: '#fff',
  },
  inputField: {
    borderColor: '#fff',
    borderWidth: 1,
    color: '#fff',
    paddingLeft: 20,
  }
});
