import React, { Component } from 'react';
import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View
} from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

// Styles
import common from './styles/common';
import SvgUri from 'react-native-svg-uri';
import { Header } from './ui/Header';
import authService from './services/auth';
import { AsyncStorage } from 'react-native';
import { AxiosResponse } from 'axios';
import PopupMessageComponent from './ui/PopupMessage';
import { User } from './interfaces/user';
import * as ImageManipulator from 'expo-image-manipulator';
import * as DocumentPicker from 'expo-document-picker';
import { BigField } from './ui/BigField';

import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

interface State {
  error: string,
  errors: {
    [key: string]: string;
  }
  user?: Partial<User>,
  tab: 'info' | 'contacts';
}

export class ProfileScreen extends Component<Props, State> {
  private initialState: Partial<User>;

  public constructor(props: Props) {
    super(props);

    this.state = {
      error: '',
      errors: {},
      tab: 'info',
    };
  }

  public async componentDidMount(): Promise<void> {
    await this.fetchUser();
  }

  public render(): React.ReactNode {
    return (
      <View style={{ flex: 1 }}>

        {/* Header */}
        <Header
          title={'Профиль'}
          button={this.renderButton()}
          goBack={this.props.navigation.goBack}
          back
        />

        {/* Profile */}
        {this.renderProfile()}

        {/* Popup */}
        {this.renderPopup()}

      </View>
    );
  }

  private renderProfile = (): React.ReactNode => {
    if (!this.state.user) {
      return null;
    }

    return (
      <KeyboardAvoidingView style={{ flex: 1, width: '100%' }} behavior='padding' enabled>

        <ScrollView style={styles.profile} contentContainerStyle={{
          flexGrow: 1,
          justifyContent: 'space-between'
        }}>

          <View style={{ padding: '8%' }}>

            {/* Picture block */}
            <View style={styles.pictureBlock}>

              {/* Picture */}
              {this.renderPicture()}

              {/* Text */}
              <View style={styles.changePhotoWrapper} onTouchEnd={this.openImagePicker}>
                <Text style={styles.changePhotoText}>Изменить фото</Text>
                <SvgUri
                  height={14}
                  width={14}
                  source={require('./img/pen.svg')}
                />
              </View>

            </View>

            {/* Toggle */}
            {this.renderToggleButton()}

            {/* Info */}
            {this.renderInfo()}

            {/* Contacts */}
            {this.renderContacts()}

          </View>

        </ScrollView>

      </KeyboardAvoidingView>
    );
  };

  private renderPicture = (): React.ReactNode => {
    if (this.state.user.picture) {
      const base64 = `data:image/png;base64,${this.state.user.picture}`;

      return (
        <Image style={{ height: 100, width: 100, borderRadius: 50 }} source={{ uri: base64 }}/>
      );
    }

    return (
      <View style={styles.picture}>
        <Text>Нет фото</Text>
      </View>
    );
  };

  private renderToggleButton = (): React.ReactNode => {
    return (
      <View style={styles.toggle}>
        <View
          style={[styles.toggleTextWrapper, this.state.tab === 'info' ? styles.toggleTextWrapperActive : null]}
          onTouchEnd={this.setTab('info')}
        >
          <Text
            style={[styles.toggleText, this.state.tab === 'info' ? styles.toggleTextActive : null]}
          >Имя и
            должность</Text>
        </View>
        <View
          style={[styles.toggleTextWrapper, this.state.tab === 'contacts' ? styles.toggleTextWrapperActive : null]}
          onTouchEnd={this.setTab('contacts')}
        >
          <Text
            style={[styles.toggleText, this.state.tab === 'contacts' ? styles.toggleTextActive : null]}
          >Контактные данные</Text>
        </View>
      </View>
    );
  };

  private setTab = (tab: 'info' | 'contacts') => (): void => {
    this.setState({ tab });
  };

  /**
   * Renders the header button to pass
   * to the "Header" component.
   */
  private renderButton = (): React.ReactNode => {
    return (
      <View style={common.headerButton} onTouchEnd={this.handleSignOut}>
        <Text style={common.headerButtonText}>Выйти из аккаунта</Text>
      </View>
    );
  };

  private openImagePicker = async (): Promise<void> => {
    // Pick a single file
    try {
      const res = await DocumentPicker.getDocumentAsync({
        type: 'image/jpeg',
      });

      const base64String = await ImageManipulator.manipulateAsync(res.uri, [], { base64: true });

      await this.handleInput('picture')(base64String.base64);
      await this.handleSubmit();
    } catch (err) {
      console.log(err.response.data);
    }
  };

  private renderPopup = (): React.ReactNode => {
    if (!this.state.error) {
      return null;
    }

    return (
      <PopupMessageComponent
        type={'error'}
        text={this.state.error}
        close={this.unsetError}
      />
    );
  };

  private fetchUser = async (): Promise<void> => {
    try {
      const response = await authService.profile() as AxiosResponse<User>;

      this.setState({ user: response.data });
      this.initialState = response.data;
    } catch (err) {
      console.error(err);
    }
  };

  private handleSignOut = async (): Promise<void> => {
    try {
      const response = await authService.logOut();

      if (response.status === 200) {
        await AsyncStorage.clear();

        const { navigate } = this.props.navigation;
        navigate('Home');
      }
    } catch (err) {
      this.setState({ error: err.response.data.message });
    }
  };

  private handleInput = (field: string) => (text: string): Promise<void> => {
    return new Promise<void>(((resolve, _) => {
      this.setState({ user: { ...this.state.user, [field]: text } }, resolve);
    }));
  };

  private handleRadio = async (value: string): Promise<void> => {
    await this.handleInput('gender')(value);
    await this.handleSubmit();
  };

  private unsetError = (): void => {
    this.setState({ error: '' });
  };

  private handleSubmit = async (): Promise<void> => {
    const userClone = { ...this.state.user };

    for (const key in userClone) {
      if (!userClone[key]) {
        delete userClone[key];
      }
    }

    try {
      const response = await authService.updateProfile(userClone) as AxiosResponse<User>;

      this.setState({ user: response.data });
    } catch (err) {
      this.setState({ user: this.initialState });

      if (err.response.status === 422) {
        this.setState({ errors: err.response.data.errors });
      } else {
        this.setState({ error: err.message });
      }
    }
  };

  private renderInfo = (): React.ReactNode => {
    if (this.state.tab !== 'info') {
      return null;
    }

    return (
      <>
        <View style={styles.field}>
          <Text style={styles.fieldSub}>Имя</Text>
          <TextInput
            onChangeText={this.handleInput('first_name')}
            onBlur={this.handleSubmit}
            value={this.state.user.first_name}
            style={styles.fieldInput}
          />
        </View>

        <View style={styles.field}>
          <Text style={styles.fieldSub}>Фамилия</Text>
          <TextInput
            onChangeText={this.handleInput('last_name')}
            onBlur={this.handleSubmit}
            value={this.state.user.last_name}
            style={styles.fieldInput}
          />
        </View>

        <View style={[styles.field, styles.genderField]}>
          <Text style={styles.fieldSub}>Пол</Text>
          <RadioForm
            animation={false}
            formHorizontal={true}
            initial={this.state.user.gender}
          >
            <RadioButton labelHorizontal={true} wrapStyle={{ marginRight: 27 }} key={0}>
              <RadioButtonInput
                obj={{ label: 'Мужчина', value: 'male' }}
                index={0}
                isSelected={this.state.user.gender === 'male'}
                onPress={this.handleRadio}
                buttonSize={14}
              />
              <RadioButtonLabel
                obj={{ label: 'Мужчина', value: 'male' }}
                index={0}
                labelHorizontal={true}
                onPress={this.handleRadio}
                labelStyle={{ fontSize: 14 }}
                labelWrapStyle={{}}
              />
            </RadioButton>
            <RadioButton labelHorizontal={true} key={1}>
              <RadioButtonInput
                obj={{ label: 'Женщина', value: 'female' }}
                index={0}
                isSelected={this.state.user.gender === 'female'}
                onPress={this.handleRadio}
                buttonSize={14}
              />
              <RadioButtonLabel
                obj={{ label: 'Женщина', value: 'female' }}
                index={0}
                labelHorizontal={true}
                onPress={this.handleRadio}
                labelStyle={{ fontSize: 14 }}
                labelWrapStyle={{ marginRight: 27 }}
              />
            </RadioButton>
          </RadioForm>
        </View>

        <View style={styles.field}>
          <Text style={styles.fieldSub}>Компания</Text>
          <TextInput
            onChangeText={this.handleInput('company')}
            onBlur={this.handleSubmit}
            value={this.state.user.company}
            style={styles.fieldInput}
          />
        </View>

        <View style={styles.field}>
          <Text style={styles.fieldSub}>Должность</Text>
          <TextInput
            onChangeText={this.handleInput('position')}
            onBlur={this.handleSubmit}
            value={this.state.user.position}
            style={styles.fieldInput}
          />
        </View>
      </>
    );
  };

  private renderContacts = (): React.ReactNode => {
    if (this.state.tab !== 'contacts') {
      return null;
    }

    return (
      <>
        <BigField
          title={'Ваш Телефон'}
          field={'phone'}
          user={this.state.user}
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
        />

        <BigField
          title={'Ваш E-Mail (является логином в систему. Помните об этом при его изменении)'}
          field={'email'}
          user={this.state.user}
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
        />

        <BigField
          title={'Ваш пароль'}
          field={'password'}
          user={this.state.user}
          placeholder={'******'}
          secure
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
        />

      </>
    );
  };
}

const styles = StyleSheet.create({
  field: {
    marginTop: 14,
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
  },
  genderField: {
    borderBottomWidth: 0,
  },
  bigField: {
    height: 56,
    borderRadius: 28,
  },
  fieldSub: {
    marginBottom: 14,
    fontSize: 14,
    color: '#A8A8A8',
  },
  fieldInput: {
    fontSize: 16,
    paddingBottom: 6,
  },
  toggle: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 32,
    padding: 3,
    height: 48,
    width: '100%',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 24,
  },
  toggleTextWrapper: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  toggleText: {
    color: '#727374',
    fontSize: 13,
  },
  toggleTextWrapperActive: {
    backgroundColor: '#0082FD',
    borderRadius: 21,
  },
  toggleTextActive: {
    color: 'white',
  },
  profile: {
    // Sizes
    width: '100%',
  },
  pictureBlock: {
    display: 'flex',
    alignItems: 'center',
  },
  picture: {
    // Flex
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    // Size
    height: 100,
    width: 100,

    // Styling
    borderRadius: 50,
    backgroundColor: '#E6E6E6',
  },
  changePhotoWrapper: {
    // Flex
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',

    // Margins
    marginTop: 11,
  },
  changePhotoText: {
    marginRight: 8,
    fontSize: 16,
    color: '#2858C9',
  },
  genderRadio: {
    display: 'flex',
    flexDirection: 'row',
  },
});
